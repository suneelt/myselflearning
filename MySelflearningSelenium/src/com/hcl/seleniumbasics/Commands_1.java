package com.hcl.seleniumbasics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Commands_1 {

	public static void main(String[] args) {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://demoqa.com/frames-and-windows/");
		driver.findElement(By.xpath("//a[text()='New Browser Tab']")).click();
		driver.close();
	}

}
