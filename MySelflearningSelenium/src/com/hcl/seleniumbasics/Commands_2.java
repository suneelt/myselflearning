package com.hcl.seleniumbasics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Commands_2 {

	public static void main(String[] args) {

		WebDriver driver=new FirefoxDriver();

		String url="http://www.store.demoqa.com";

		driver.get(url);

		String pagetitle=driver.getTitle();

		int pagetitlelength=pagetitle.length();

		System.out.println("Page Tiltle is : " +pagetitle +"Page title length is :"+pagetitlelength);

		String actualurl=driver.getCurrentUrl();

		if(actualurl.equals(url)) {
			System.out.println("Verification successfull.The correct url is opened");
		}else {
			System.out.println("Verification failed.The wrong ulr is opened");
		}

		String pagesource=driver.getPageSource();
		int pagesorcelength=pagesource.length();

		System.out.println("The length of the page source is :" +pagesorcelength );

		driver.close();


	}

}
